﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using FibonacciClient.Models;

namespace FibonacciClient.ViewModels
{
    public class FibonacciCalculationViewModel
    {
        private readonly FibonacciCalculation _result;

        public FibonacciCalculationViewModel(FibonacciCalculation result)
        {
            _result = result;
        }

        public long Number => _result.Number;
        public BigInteger Result => _result.Result;
    }
}
