﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Numerics;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using FibonacciClient.Models;
using ReactiveUI;
using ReactiveUI.Legacy;
using ReactiveCommand = ReactiveUI.ReactiveCommand;

namespace FibonacciClient.ViewModels
{
    public class AppViewModel : ReactiveObject//, ISupportsValidation
    {

        //Backing field and property for text field with Fibonacci number to find.
        private string _noInSeq;
        public string NoInSeq
        {
            get => _noInSeq;
            set => this.RaiseAndSetIfChanged(ref _noInSeq, value);
        }

        /// Command that calculates the n:th fibonacci number in sequence, as defined by input.
        public ReactiveCommand<string, FibonacciCalculation> ExecuteCalculation { get; } = ReactiveCommand.CreateFromTask<string, FibonacciCalculation>(
            noInSeq => GetCalculationResultsFromServer(long.Parse(noInSeq)));

        //Collections to hold raw result of calculations and derived collection to hold ViewModel for results.
        public ReactiveList<FibonacciCalculation> CalculationResults { get; } = new ReactiveList<FibonacciCalculation>();
        public IReactiveDerivedList<FibonacciCalculationViewModel> CalculationResultsViewModels { get; protected set; }


        //Spinner to show that the app is processing latest input.
        private readonly ObservableAsPropertyHelper<Visibility> _spinnerVisibility;
        public Visibility SpinnerVisibility => _spinnerVisibility.Value;

        public AppViewModel()
        {
            //Set up derived collection to show results from calculations.
            CalculationResultsViewModels = CalculationResults.CreateDerivedCollection(result => new FibonacciCalculationViewModel(result));

            //Bind input field to start calculations with validation and debounce. Not very logical way to do it in this case, but it was fun to try. :)
            ulong _;
            this.WhenAnyValue(x => x.NoInSeq)
                .Throttle(TimeSpan.FromMilliseconds(1200), RxApp.MainThreadScheduler)
                .Select(x => x?.Trim())
                .DistinctUntilChanged()
                .Where(x => ulong.TryParse(NoInSeq, out _))
                .InvokeCommand(ExecuteCalculation);

            //Bind the visibility of spinner to execution status of calculation command.
            _spinnerVisibility = ExecuteCalculation.IsExecuting
                .Select(x => x ? Visibility.Visible : Visibility.Collapsed)
                .ToProperty(this, x => x.SpinnerVisibility, Visibility.Hidden);

            //Subscribe to errors in calculations and handle them.
            ExecuteCalculation.ThrownExceptions.Subscribe(ex => {/* Handle errors here */});

            //Subscribe to results from calculation and add them to the results set.
            ExecuteCalculation.Subscribe(newResult => CalculationResults.Add(newResult));
        }


        public static async Task<FibonacciCalculation> GetCalculationResultsFromServer(long noInSeq)
        {
            var client = new FibonacciServer.FibonacciServerClient();
            var res = await client.GiveMeTheNthFibonacciNoLargeAsync(noInSeq);

            return new FibonacciCalculation(noInSeq, res);
        }

    }
}
