﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciClient.Models
{
    public class FibonacciCalculation
    {
        public FibonacciCalculation(long number, long result)
        {
            Number = number;
            Result = new BigInteger(result);
        }

        public FibonacciCalculation(long number, BigInteger result)
        {
            Number = number;
            Result = result;
        }

        public long Number { get; }
        public BigInteger Result { get; }
    }
}
