﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FibonacciClient.Validators
{
    /// <summary>
    /// Validates integer values. Verifes value and asserts that it is between min and max.
    /// </summary>
    public class IntRangeRule : ValidationRule
    {
        /// <summary>
        /// Min allowed value for the bound property
        /// </summary>
        public int Min { get; set; }
        /// <summary>
        /// Max allowed value for the bound property
        /// </summary>
        public int Max { get; set; }

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            int input;
            try
            {
                if(value == null)
                    return new ValidationResult(false, "Enter an integer value!");

                //Check if value is empty and if so return a erro.
                var stringValue = value.ToString();
                if(string.IsNullOrWhiteSpace(stringValue))
                    return new ValidationResult(false, "Enter an integer value!");

                //Use exception handling to check for erros in parsing since it
                // gives better error than TryParse.
                input = int.Parse(value.ToString());
            }
            catch (FormatException)
            {
                return new ValidationResult(false, "Enter an integer value!");
            }
            catch (ArgumentNullException ex)
            {
                return new ValidationResult(false, "Invalid value! " + ex.Message);
            }
            catch (OverflowException ex)
            {
                return new ValidationResult(false, "Invalid value! " + ex.Message);
            }

            //Check if the value is within the defined limits.
            if (input < Min || input > Max)
            {
                return new ValidationResult(false, "Enter an integer value between " + Min + " and " + Max + ".");
            }

            //All checks succeeded!
            return new ValidationResult(true, null);
        }
    }
}
