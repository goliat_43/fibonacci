﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FibonacciServer
{
    public class FibonacciServer : IFibonacciServer
    {

        /// <summary>
        /// Calculates the Nth number of the Fibonacci series limited to values that fit in a long.
        /// </summary>
        /// <param name="noInSeq">What number in the Fibonacci series to calculate.</param>
        /// <returns>The Fibonacci number corresponding to the specified number.</returns>
        public long GiveMeTheNthFibonacciNo(long noInSeq)
        {
            try
            {
                return (long)GiveMeTheNthFibonacciNoLarge(noInSeq);
            }
            catch (OverflowException)
            {
                throw new FaultException($"Specified Fibonacci number {noInSeq} is too large. You shuld use GiveMeTheNthFibonacciNoLarge instead!");
            }
        }

        /// <summary>
        /// Calculates the Nth number of the Fibonacci series.
        /// </summary>
        /// <param name="noInSeq">What number in the Fibonacci series to calculate.</param>
        /// <returns>The Fibonacci number corresponding to the specified number.</returns>
        public BigInteger GiveMeTheNthFibonacciNoLarge(long noInSeq)
        {
            try
            {
                var fibonacciCalculator = new FibonacciCalculator();
                return fibonacciCalculator.CalculateFibonacci(noInSeq);
            }
            catch (InsufficientMemoryException)
            {
                throw new FaultException($"Out of memory! Specified Fibonacci number {noInSeq} is too large.");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                throw new FaultException(ex.Message);
            }
        }
    }
}
