﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FibonacciServer
{
    [ServiceContract]
    [ServiceKnownType(typeof(BigInteger))]
    public interface IFibonacciServer
    {

        [OperationContract]
        long GiveMeTheNthFibonacciNo(long noInSeq);

        [OperationContract]
        BigInteger GiveMeTheNthFibonacciNoLarge(long noInSeq);
    }
}
