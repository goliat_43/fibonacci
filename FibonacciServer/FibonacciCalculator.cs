﻿using System;
using System.Numerics;

namespace FibonacciServer
{
    public class FibonacciCalculator
    {
        /// <summary>
        /// Caclulates the n:th Fibonacci number.
        /// </summary>
        /// <param name="noInSeq"></param>
        /// <returns></returns>
        public BigInteger CalculateFibonacci(long noInSeq)
        {
            if (noInSeq < 0) throw new ArgumentOutOfRangeException(nameof(noInSeq), noInSeq, "Negative values are not allowed for Fibonacci calculation!");
            if (noInSeq == 0) return 0;

            /* Use that Fibonacci number can be represented on matrix form as:
             *
             *  |1  1|^n   |F_n+1   F_n  |
             *  |    |   = |             |
             *  |1  0|     |Fn      F_n-1|
             *  
             *  And solve the system for Fn using matrix multiplication.
             */

            BigInteger[][] F = {new BigInteger[] {1,1}, new BigInteger[] {1,0}};

            RaiseFibonacciMatrixToPower(F, noInSeq-1);
      
            return F[0][0];
        }

        /// <summary>
        /// Do "in place" matrix multiplication in F. Calculates FM and places result in F.
        /// </summary>
        /// <param name="F">2by2 matrix to be multiplied. Replaces with result! </param>
        /// <param name="M">2by2 matrix to be multiplied</param>
        private void MatrixMultiplication(BigInteger[][] F, BigInteger[][] M)
        {
            //Calculate new matrix according to mathematical rules for 2 by 2 matrix multiplication.
            var x = F[0][0] * M[0][0] + F[0][1] * M[1][0];
            var y = F[0][0] * M[0][1] + F[0][1] * M[1][1];
            var z = F[1][0] * M[0][0] + F[1][1] * M[1][0];
            var w = F[1][0] * M[0][1] + F[1][1] * M[1][1];

            //Replace F with with multiplied rsult.
            F[0][0] = x;
            F[0][1] = y;
            F[1][0] = z;
            F[1][1] = w;
        }

        /// <summary>
        /// Raises matrix to the power n. Optimizations only applicable for Fibonacci calculation.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="n"></param>
        private void RaiseFibonacciMatrixToPower(BigInteger[][] matrix, long n)
        {
            if (n == 0 || n == 1)
                return;

            BigInteger[][] M = { new BigInteger[] {1,1}, new BigInteger[] {1,0}};

            RaiseFibonacciMatrixToPower(matrix, n/2);
            MatrixMultiplication(matrix, matrix);
      
            if (n%2 != 0)
                MatrixMultiplication(matrix, M);
        }
    }
}