﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FibonacciServer;
using NUnit.Compatibility;

namespace FibonacciServerFixture
{
    [TestFixture]
    public class FibonacciServerFixture
    {
        public class FibonacciInRangeOfLong
        {
            //OC(<Fibonacci number to calulate>, <Expected result>)
            private static readonly object[] NormalFibonacciSeries =
            {
                OC(0, 0),
                OC(1, 1),
                OC(2, 1),
                OC(3, 2),
                OC(4, 3),
                OC(5, 5),
                OC(6, 8),
                OC(14, 377),
                OC(20, 6765),
                OC(92, 7540113804746346429),
            };

            [Test, TestCaseSource(nameof(NormalFibonacciSeries))]
            public void GiveMeTheNthFibonacciNo_GivenNormalNumberNumberInSequence_ReturnsCorrectResult(
                long numberInSequence,
                long expectedResult)
            {
                var target = new FibonacciServer.FibonacciServer();

                var res = target.GiveMeTheNthFibonacciNo(numberInSequence);

                Assert.That(res, Is.EqualTo(expectedResult));
            }

            [Test, TestCaseSource(nameof(NormalFibonacciSeries))]
            public void GiveMeTheNthFibonacciNoLarge_GivenNormalNumberNumberInSequence_ReturnsCorrectResult(
                long numberInSequence,
                long expectedResult)
            {
                var target = new FibonacciServer.FibonacciServer();

                var res = target.GiveMeTheNthFibonacciNoLarge(numberInSequence);

                Assert.That(res, Is.EqualTo(new BigInteger(expectedResult)));
            }
        }

        public class FibonacciTooLargeForLong
        {
            //OCB(<Fibonacci number to calulate>, <Expected result as string>)
            private static readonly object[] ExtendedFibonacciSeries =
            {
                OCB(100, "354224848179261915075"),
                OCB(213, "146178119651438213260386312206974243796773058"),
                OCB(500, "139423224561697880139724382870407283950070256587697307264108962948325571622863290691557658876222521294125"),
            };

            [Test, TestCaseSource(nameof(ExtendedFibonacciSeries))]
            public void GiveMeTheNthFibonacciNo_GivenExtendedNumberNumberInSequence_ThrowsFaultException(
                long numberInSequence,
                BigInteger expectedResult)
            {
                var target = new FibonacciServer.FibonacciServer();

                Assert.That(() => target.GiveMeTheNthFibonacciNo(numberInSequence),
                            Throws.InstanceOf(typeof(FaultException)));
            }

            [Test, TestCaseSource(nameof(ExtendedFibonacciSeries))]
            public void GiveMeTheNthFibonacciNoLarge_GivenExtendedNumberNumberInSequence_ReturnsCorrectResult(
                long numberInSequence,
                BigInteger expectedResult)
            {
                var target = new FibonacciServer.FibonacciServer();

                var res = target.GiveMeTheNthFibonacciNoLarge(numberInSequence);

                Assert.That(res, Is.EqualTo((BigInteger) expectedResult));
            }
        }
        
        private static readonly long[] NegativeSequenceNumbers =
        {
            -1,
            -2,
            -1337,
            -5672382923749,
            long.MinValue,
        };

        [Test, TestCaseSource(nameof(NegativeSequenceNumbers))]
        public void GiveMeTheNthFibonacciNo_GivenNegativeNumber_ThrowsFaultException(long negativeNumber)
        {
            var target = new FibonacciServer.FibonacciServer();

            Assert.That(() => target.GiveMeTheNthFibonacciNo(negativeNumber), Throws.InstanceOf(typeof(FaultException)));
        }

        #region Helpers
        /// <summary>
        /// Generates a test result set for the Fibonacci series.
        /// </summary>
        private static object[] OC(long numInSequence, long expectedResult)
        {
            return new object[] { numInSequence, expectedResult };
        }

        private static object[] OCB(long numInSequence, string expectedResult)
        {
            return new object[] { numInSequence, BigInteger.Parse(expectedResult) };
        }
        #endregion Helpers
    }
}
